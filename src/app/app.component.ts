import { Component, OnInit } from '@angular/core';
import { CarsService } from './cars.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [CarsService]
})

export class AppComponent implements OnInit {
  headline = 'Сравнение';
  cars: Car[];

  isHideClimateControl = false;
  isHideConditioner = false;
  isHideAlloyWheels = false;
  isHideMultiWheel = false;
  isHideParktronic = false;
  isHideAbs = false;
  isHideAlarm = false;
  isHideFullServiceHistroy = false;
  isHideAsterPlusExpert = false;

  constructor(private carsService: CarsService) {}

  ngOnInit() {
    this.getCars();
  }

  getCars() {
    this.cars = this.carsService.getCars();
  }

  hideOptions() {
    this.isHideClimateControl = _.every(this.cars, ['climateControl', true]);
    this.isHideConditioner = _.every(this.cars, ['conditioner', true]);
    this.isHideAlloyWheels = _.every(this.cars, ['alloyWheels', true]);
    this.isHideMultiWheel = _.every(this.cars, ['multiWheel', true]);
    this.isHideParktronic = _.every(this.cars, ['parktronic', true]);
    this.isHideAbs = _.every(this.cars, ['abs', true]);
    this.isHideAlarm = _.every(this.cars, ['alarm', true]);
    this.isHideFullServiceHistroy = _.every(this.cars, ['fullServiceHistroy', true]);
    this.isHideAsterPlusExpert = _.every(this.cars, ['asterPlusExpert', true]);
  }

  showOptions() {
    this.isHideClimateControl = false;
    this.isHideConditioner = false;
    this.isHideAlloyWheels = false;
    this.isHideMultiWheel = false;
    this.isHideParktronic = false;
    this.isHideAbs = false;
    this.isHideAlarm = false;
    this.isHideFullServiceHistroy = false;
    this.isHideAsterPlusExpert = false;
  }

  // init slick carousel
  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "infinite": true,
    "responsive": [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 930,
        settings: {
          slidesToShow: 2
        }
      },{
        breakpoint: 700,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };

  showInfo(carName) {
    console.info('Название автомобиля: ', carName);
  }

}

interface Car {
  id: number;
  imgUrl: string;
  modelName: string
  oldPrice: string;
  currentPrice: string;
  mileage: string;
  carcase: string;
  engine: string;
  driveUnit: string;
  climateControl: any;
  conditioner: any;
  alloyWheels: any;
  multiWheel: any;
  parktronic: any;
  abs: any;
  alarm: any;
  fullServiceHistroy: any;
  asterPlusExpert: any;
  salon: string;
  workingHours: string;
  phone: string;
}
