import { EvoAppPage } from './app.po';

describe('evo-app App', () => {
  let page: EvoAppPage;

  beforeEach(() => {
    page = new EvoAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
