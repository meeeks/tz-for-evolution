import {Injectable} from '@angular/core';
import {CARS} from './mock-cars';

@Injectable()
export class CarsService {
  getCars() {
    return CARS;
  }
}
